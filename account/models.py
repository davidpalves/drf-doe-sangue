from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    TIPOS_DE_SANGUE_CHOICES = (
        ('a+', 'A+'),
        ('b+', 'B+'),
        ('ab+', 'AB+'),
        ('o+', 'O+'),
        ('a-', 'A-'),
        ('b-', 'B-'),
        ('ab-', 'AB-'),
        ('o-', 'O-'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tipo_sanguineo = models.CharField(
        max_length=3, choices=TIPOS_DE_SANGUE_CHOICES, blank=True)
    data_nascimento = models.DateField(null=True, blank=True)
    local = models.CharField(max_length=100, blank=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
