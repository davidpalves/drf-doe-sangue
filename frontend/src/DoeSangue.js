import axios from 'axios';
const API_URL = 'http://localhost:8000';

export default class DoeSangue{

    getSangueListByURL(link){
        const url = `${API_URL}${link}`;
        return axios.get(url).then(response => response.data)
    }

}