from django.db import models


class NivelSangue(models.Model):
    banco = models.CharField(max_length=255,)
    tipo_sangue = models.CharField(max_length=255,)
    nivel_sangue = models.CharField(max_length=255,)
    url = models.CharField(max_length=255,)
    cidade = models.CharField(max_length=255, )
    scrapedat = models.DateField()

    class Meta:
        db_table = 'nivel_sangue'
