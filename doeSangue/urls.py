from django.urls import path
from doeSangue import views

urlpatterns = [
    path('', views.NivelSangueList.as_view())
]
