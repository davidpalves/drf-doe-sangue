from django_filters.rest_framework import DjangoFilterBackend
from doeSangue.serializers import NivelSangueSerializer
from rest_framework import generics
from .models import NivelSangue
import django_filters


class NivelSangueFilter(django_filters.FilterSet):
    banco = django_filters.CharFilter(
        field_name='banco',
        lookup_expr='iexact'
        )

    tipo_sangue = django_filters.CharFilter(
        field_name='tipo_sangue',
        lookup_expr='icontains'
        )

    cidade = django_filters.CharFilter(
        field_name='cidade',
        lookup_expr='iexact'
        )

    class Meta:
        model = NivelSangue
        fields = ['banco', 'tipo_sangue', 'cidade']


class NivelSangueList(generics.ListAPIView):
    queryset = NivelSangue.objects.all()
    serializer_class = NivelSangueSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = NivelSangueFilter
