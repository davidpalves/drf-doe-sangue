from rest_framework import serializers
from .models import NivelSangue


class NivelSangueSerializer(serializers.ModelSerializer):
    class Meta:
        model = NivelSangue
        fields = (
            'banco',
            'tipo_sangue',
            'nivel_sangue',
            'url',
            'cidade',
            'scrapedat'
            )
