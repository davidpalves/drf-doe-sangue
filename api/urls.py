from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.documentation import include_docs_urls
from django.urls import path, include
from django.contrib import admin


urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', include_docs_urls(title='Doe Sangue API')),
    path('', include('doeSangue.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)
